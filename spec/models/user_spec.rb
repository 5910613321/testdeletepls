require 'rails_helper'
#rspec for test when create user.=> signup
describe User do
    fixtures :users
    before :each do
        @user_test = users(:anna)
        #puts @user_test.class.name
    end
    
    it "is valid with valid attributes" do
        expect(@user_test).to be_valid
    end
    
    it "is not valid without a username" do 
        @user_test.email = nil
        expect(@user_test).to_not be_valid
    end
    
    
    it "is not valid without a password_digest" do 
        @user_test.password_digest = nil
        expect(@user_test).to_not be_valid
    end
    
    
end