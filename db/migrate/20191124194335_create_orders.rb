class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.string :name
      t.string :shoename
      t.string :address

      t.timestamps
    end
  end
end
