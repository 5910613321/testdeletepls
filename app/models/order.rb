class Order < ApplicationRecord
    validates :name, presence: true
    validates :shoename, presence: true
    validates :address, presence: true
end
